SET NAMES utf8;
SET time_zone = '+00:00';

CREATE DATABASE `war2co_analytics` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `war2co_analytics`;

DROP TABLE IF EXISTS `archive_total_stats`;
CREATE TABLE `archive_total_stats` (
  `this_month_minutes_online` int(11) DEFAULT '0',
  `this_month_logins` int(11) DEFAULT '0',
  `this_month_users` int(11) DEFAULT '0',
  `this_month_games` int(11) DEFAULT '0',
  `this_month_avg_online` int(11) DEFAULT '0',
  `this_month_minutes_ingame` int(11) DEFAULT '0',
  `month_year` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `country_stats`;
CREATE TABLE `country_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(50) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `game_info`;
CREATE TABLE `game_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gametime` datetime NOT NULL,
  `mapfile` varchar(100) NOT NULL,
  `tileset` varchar(100) NOT NULL,
  `joins` int(10) NOT NULL,
  `maxplayers` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `gametype` varchar(100) NOT NULL,
  `clienttag` varchar(10) NOT NULL,
  `gameoption` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mapauth` varchar(100) NOT NULL,
  `started` text NOT NULL,
  `created` text NOT NULL,
  `ended` text NOT NULL,
  `mapsize` varchar(100) NOT NULL,
  `gamereport` text NOT NULL,
  `winners` int(10) NOT NULL DEFAULT '0',
  `losers` int(10) NOT NULL DEFAULT '0',
  `discers` int(10) NOT NULL DEFAULT '0',
  `drawers` int(10) NOT NULL DEFAULT '0',
  `watchers` int(10) NOT NULL DEFAULT '0',
  `players` int(10) NOT NULL DEFAULT '0',
  `gr_link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `hack_history`;
CREATE TABLE `hack_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `ah_status` varchar(100) NOT NULL DEFAULT 'Unknown',
  `proof` text,
  `gametime` datetime DEFAULT NULL,
  `server` varchar(100) NOT NULL DEFAULT 'server.war2.ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `total_stats`;
CREATE TABLE `total_stats` (
  `ah_hack` int(11) NOT NULL DEFAULT '0',
  `ah_unknown` int(11) NOT NULL DEFAULT '0',
  `ah_none` int(11) NOT NULL DEFAULT '0',
  `ah_enabled` int(11) NOT NULL DEFAULT '0',
  `all_logins` int(11) NOT NULL DEFAULT '0',
  `all_minutes_online` int(11) NOT NULL DEFAULT '0',
  `all_avg_online` int(11) NOT NULL DEFAULT '0',
  `all_users` int(11) NOT NULL DEFAULT '0',
  `all_games` int(11) NOT NULL DEFAULT '0',
  `all_minutes_ingame` int(11) NOT NULL DEFAULT '0',
  `this_month_logins` int(11) NOT NULL DEFAULT '0',
  `this_month_minutes_online` int(11) NOT NULL DEFAULT '0',
  `this_month_avg_online` int(11) NOT NULL DEFAULT '0',
  `this_month_users` int(11) NOT NULL DEFAULT '0',
  `this_month_games` int(11) NOT NULL DEFAULT '0',
  `this_month_minutes_ingame` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tracker`;
CREATE TABLE `tracker` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `login` datetime DEFAULT NULL,
  `logout` datetime DEFAULT NULL,
  `duration_in_min` int(11) NOT NULL DEFAULT '1',
  `server` varchar(100) DEFAULT 'server.war2.ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_game_score`;
CREATE TABLE `user_game_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `structures_razed` int(10) NOT NULL,
  `structures_constructed` int(10) NOT NULL,
  `structures_lost` int(10) NOT NULL,
  `gold_mined` int(10) NOT NULL,
  `lumber_harvested` int(10) NOT NULL,
  `oil_harvested` int(10) NOT NULL,
  `total_spent` int(10) NOT NULL,
  `units_killed` int(10) NOT NULL,
  `units_produced` int(10) NOT NULL,
  `units_lost` int(10) NOT NULL,
  `overall_resources` int(10) NOT NULL,
  `overall_structures` int(10) NOT NULL,
  `overall_units` int(10) NOT NULL,
  `overall_score` int(10) NOT NULL,
  `race` varchar(50) NOT NULL,
  `gameid` int(11) NOT NULL,
  `min_in_game` int(11) NOT NULL DEFAULT '0',
  `outcome` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_stats`;
CREATE TABLE `user_stats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `total_logins` int(11) DEFAULT '0',
  `total_online_min` int(11) DEFAULT '0',
  `total_min_in_game` int(11) DEFAULT NULL,
  `avg_online` int(11) DEFAULT '0',
  `server_stats` text CHARACTER SET utf8,
  `server_profile` text CHARACTER SET utf8,
  `server` varchar(100) CHARACTER SET utf8 DEFAULT 'server.war2.ru',
  `country` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'Unknown',
  `ah_status` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'Unknown',
  `profile_img` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT 'user-default.png',
  `normal_wins` int(11) NOT NULL DEFAULT '0',
  `normal_losses` int(11) NOT NULL DEFAULT '0',
  `normal_disces` int(11) NOT NULL DEFAULT '0',
  `ladder_wins` int(11) NOT NULL DEFAULT '0',
  `ladder_losses` int(11) NOT NULL DEFAULT '0',
  `ladder_disces` int(11) NOT NULL DEFAULT '0',
  `iron_wins` int(11) NOT NULL DEFAULT '0',
  `iron_losses` int(11) NOT NULL DEFAULT '0',
  `iron_disces` int(11) NOT NULL DEFAULT '0',
  `sex` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created` datetime NOT NULL,
  `clan` varchar(100) CHARACTER SET utf8 NOT NULL,
  `rank` varchar(100) CHARACTER SET utf8 NOT NULL,
  `location` text CHARACTER SET utf8 NOT NULL,
  `ver` varchar(100) CHARACTER SET utf8 NOT NULL,
  `client` varchar(100) CHARACTER SET utf8 NOT NULL,
  `just_profile` text CHARACTER SET utf8 NOT NULL,
  `profile_country` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ladder_rating` varchar(100) CHARACTER SET utf8 NOT NULL,
  `iron_rating` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

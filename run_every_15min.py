#!/usr/bin/python

# USED FOR TCP IP COMMUNICATION
import socket

# USED FOR SLEEP
import time

# USED TO CHECK CWD
import os

# USED FOR RANDOM CLAN TAG GENERATOR
import string
import random

# MYSQL DB CONNECTOR
import MySQLdb

# UPDATE AH_ENABLED, AH_UNKNOWN, AH_NONE NUMBERS

# IMPORT CONFIG / PASSWORDS
from db_config import *




####################################
# CHECK ADMINS ONLINE WITH /ADMINS #
####################################


# ADMIN CHECKER BY USA~Archer

# RANDOM TAG GENERATOR FUNCTION
def RANDOM_CHAN_TAG(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# GENERATE RANDOM CLAN TAG
CHAN = RANDOM_CHAN_TAG()

# HARD CODED VARIABLES
BUFFER_SIZE =  2048
PORT = 6112

# SECONDS TO SLEEP BETWEEN COMMANDS TO EVADE FLOOD DISC
SLEEP_NUMBER = 5

# GET SERVER
SERVER = "server.war2.ru"

###############
# GET /admins #
###############


try:
    # DEFINE SOCKET AS TCP IP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # CONNECT TO SERVER
    s.connect((SERVER, 6112))
    # LOG IN: HIT ENTER TO INTERACT WITH THE TERMINAL
    s.send("\r\n")
    # TYPE USERNAME
    s.send(BOT_USERNAME)
    # HIT ENTER TO SEND USERNAME
    s.send("\r\n")
    # TYPE PASSWORD
    s.send(BOT_PASSWORD)
    # HIT ENTER TO SEND PASSWORD
    s.send("\r\n")
    # RECIEVE DATA BACK
    data = s.recv(BUFFER_SIZE)
    data = s.recv(BUFFER_SIZE)

    # IF SERVER SAYS LOGIN FAILED
    if "failed" in data:
        s.close()
        print "\n[!] ERROR: Username/password incorrect.\n"

    elif "no bot" in data:
        s.close()
        print "\n[!] ERROR: Server says 'Account has no bot access'\n"

# IF ERROR OCCURED EITHER WHILE CONNECTING OR RECEVING DATA BACK, DO THIS
except:

    # PRINT ERROR MESSAGE
    print "\n[!] ERROR: Could not connect to server: %s \nCheck if the server name above is correct and online and try again.\n" % SERVER
    # TRY TO GET EXT_IP, IF FAIL PRINT AS UNKOWN

# JOIN RANDOM CHANNEL
# TYPE /join <random channel>
s.send("/join %s" % (CHAN))
# HIT ENTER TO SEND
s.send("\r\n")
# GET /admins
# TYPE /admins
# HIT ENTER TO SEND
s.send("\r\n")


while True:

    # GET ADMINS
    # TYPE /admins
    s.send("/admins")
    # HIT ENTER TO SEND
    s.send("\r\n")
    # RECIEVE DATA BACK
    data0 = s.recv(BUFFER_SIZE)


    if "flooding" in data0:
        s.close()
        print "\n[!] ERROR: You've been disconnected by the server for flooding."
        print "Try again with a higher number of seconds between attacks"
 
    elif "<" in data0:
        print "\nSTATSBOT: FILTERED MSG WITH '<'\n"
        bad = s.recv(BUFFER_SIZE)
        time.sleep(SLEEP_NUMBER)
        continue

    elif "enters]" in data0:
        print "\nSTATSBOT: FILTERED MSG WITH 'enters]'\n"
        bad = s.recv(BUFFER_SIZE)
        time.sleep(SLEEP_NUMBER)
        continue

    elif "leaves]" in data0:
        print "\nSTATSBOT: FILTERED MSG WITH 'leaves]'\n"
        bad = s.recv(BUFFER_SIZE)
        time.sleep(SLEEP_NUMBER)
        continue

    elif "is here]" in data0:
        print "\nSTATSBOT: FILTERED MSG WITH 'is here]'\n"
        bad = s.recv(BUFFER_SIZE)
        time.sleep(SLEEP_NUMBER)
        continue

    elif "Announcement" in data0:
        print "\nSTATSBOT: FILTERED MSG WITH 'Announcement'\n"
        bad = s.recv(BUFFER_SIZE)
        time.sleep(SLEEP_NUMBER)
        continue
        
    elif "Currently" not in data0:
        continue
    
    elif "Currently" in data0:
        data0 = data0.strip('Currently logged on Administrators:\n')
	data0 = data0.rstrip()
        break


s.close()

file = open("/home/war2co/scripts/admins-on-this-minute.txt", "w")
print >>file, str(data0)
file.close

###############################################
# FIND WHAT ADMINS HAVE NEVER BEEN SEE BEFORE #
###############################################

with open("/home/war2co/scripts/admins-on-all.txt", "r") as f:
    check_ifnew = f.readlines()
    new_admins = set() # holds lines already seen
    outfile = open("/home/war2co/scripts/admins-on-all.txt", "at")
    for line in open("/home/war2co/scripts/admins-on-this-minute.txt", "r"):
        if line not in check_ifnew: # not a duplicate
            new_admins.add(line)
    outfile.writelines(sorted(new_admins))
    outfile.close() 
    f.close()



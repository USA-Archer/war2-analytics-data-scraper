#!/usr/bin/python
# IMPORT WEB BROWSER
import mechanize

# IMPORT HTML PARSER
from bs4 import BeautifulSoup, NavigableString, Tag

# FOR TIME CALCS
from datetime import datetime, timedelta, date

# FOR RENAMING FILES
import os

# FOR SLEEP
import time

# FOR TIMESTAMPS
import datetime

# IMPORT MYSQL CONNECTOR
import MySQLdb

# IMPORT CONFIG / PASSWORDS
from db_config import *





x = conn.cursor()


#########################################
# SETUP MECAHNIZE TO USE AS WEB BROWSER #
#########################################

br = mechanize.Browser()
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

###########################################    
# GET "WHO'S ONLINE?" LIST FROM GAME SITE #
###########################################

try:
    # User-Agent
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko')]
    r = br.open('http://en.war2.ru/wp-content/themes/war2.ru/lib/server-stats.php')
    page = r.read()
    soup = BeautifulSoup(page)
    user_list = soup.find_all('div', class_='status-list')
except:
    print "ERROR: Connection failed."

#################################################
# ERASE USERS ONLINE THIS MINUTE FROM LAST TIME #
#################################################

with open("/home/war2co/scripts/users-on-this-minute.txt", "w") as f:
    f.close()

#########################################################
# FILTER OUT HTML AND GIVE A PLAIN LIST OF USERS ONLINE #
#########################################################

for br in user_list:
    users = br.findAll('br')

    for br in users:
        next = br.nextSibling
        if not (next and isinstance(next,NavigableString)):
            continue
        next2 = next.nextSibling
        if next2 and isinstance(next2,Tag) and next2.name == 'br':
            text = str(next.encode('latin1')).strip()
            if text:
                output = next[1:].encode('latin1')

                ####################################################
                # PRINT EACH ONLINE USER TO TEXT FILE ONE PER LINE #
                ####################################################
                
                file = open("/home/war2co/scripts/users-on-this-minute.txt", "at")
                print >>file, str(output)
                file.close
                
                
##############################################
# FIND WHAT USERS HAVE NEVER BEEN SEE BEFORE #
##############################################

with open("/home/war2co/scripts/users-on-all.txt", "r") as f:
    check_ifnew = f.readlines()
    new_users = set() # holds lines already seen
    outfile = open("/home/war2co/scripts/users-on-all-new.txt", "w")
    for line in open("/home/war2co/scripts/users-on-this-minute.txt", "r"):
        if line not in check_ifnew: # not a duplicate
            new_users.add(line)
    outfile.writelines(sorted(new_users))
    outfile.close() 
    f.close()

#################################################
# ADD NEW ROW TO user_stats table FOR NEW USERS #
#################################################

with open("/home/war2co/scripts/users-on-all-new.txt", 'r') as newuser_list:
    new_users = [line.strip() for line in newuser_list]

for username in new_users:


    x = conn.cursor()
    x.execute("INSERT into user_stats (username) values (%s)", (username))
    conn.commit()


#################################################
# APPEND NEW USERS TO USERS-ON-ALL.TXT AND SORT #
#################################################

with open("/home/war2co/scripts/users-on-all-new.txt", "r") as f:
    lines = f.readlines()
    with open("/home/war2co/scripts/users-on-all.txt", "at") as f1:
        f1.writelines(sorted(lines))
        f1.close()
    f.close()

                
############################
# CREATE NEWLY ONLINE LIST #
############################
# Creates the Newly Online list by checking the users logged on now 
# against the users logged on a minute ago. If there is a user logged on
# now who wasn't on a minute ago, he is added to newly online

with open("/home/war2co/scripts/users-on-previous-minute.txt", "r") as f:
    check_online = f.readlines()
    new_online = set() # holds lines already seen
    outfile = open("/home/war2co/scripts/users-newly-online.txt", "w")
    for line in open("/home/war2co/scripts/users-on-this-minute.txt", "r"):
        if line not in check_online: # not a duplicate
            new_online.add(line)
    outfile.writelines(sorted(new_online))
    outfile.close() 
    f.close()

#######################################################
# WRITE LOGON TIMESTAMPS TO DB FOR NEWLY ONLINE USERS #
#######################################################

with open("/home/war2co/scripts/users-newly-online.txt", 'r') as user_list:
    users_online = [line.strip() for line in user_list]

for username in users_online:


    x.execute("SELECT * FROM tracker WHERE username = %s AND logout IS NULL", (username))
    results = x.fetchall()
    for row in results:

        already_online = row[0]

    try:
        already_online

    except NameError:
    	already_online = None

    if not already_online:



        x.execute("INSERT into tracker (username, login) values (%s, UTC_TIMESTAMP())", (username))
        conn.commit()
    
       

#############################
# CREATE NEWLY OFFLINE LIST #
#############################
# Creates the Newly Offline list by checking the users logged on now 
# against the users logged on a minute ago. If there was a
# user logged on a minute ago but not now, he is added to newly offline

with open("/home/war2co/scripts/users-on-this-minute.txt", "r") as f:
    check_offline = f.readlines()
    new_offline = set() # holds lines already seen
    outfile = open("/home/war2co/scripts/users-newly-offline.txt", "w")
    for line in open("/home/war2co/scripts/users-on-previous-minute.txt", "r"):
        if line not in check_offline: # not a duplicate
            new_offline.add(line)
    outfile.writelines(sorted(new_offline))
    outfile.close()      
    f.close()

#########################################################
# WRITE LOGOFF TIMESTAMPS TO DB FOR NEWLY OFFLINE USERS #
#########################################################

with open("/home/war2co/scripts/users-newly-offline.txt", 'r') as user_list:
    users_offline = [line.strip() for line in user_list]

for username in users_offline:




    
    # WRITE LOG OFF TIMESTAMP
    x.execute("UPDATE tracker SET logout = UTC_TIMESTAMP() WHERE username = %s AND logout IS NULL", (username))
    conn.commit()

    # FIND NEW AVG ONLINE DURATION
    x.execute("SELECT AVG(duration_in_min) FROM tracker WHERE username = %s AND logout IS NOT NULL", (username))
    results = x.fetchall()
    for row in results:

        AVG_ONLINE = row[0]

    conn.commit()
    AVG_ONLINE = int(0 if AVG_ONLINE is None else AVG_ONLINE)



    # WRITE NEW AVERAGE ONLINE DURATION TO USER_STATS
    x.execute("UPDATE user_stats SET avg_online = %s WHERE username = %s", (AVG_ONLINE, username))
    conn.commit()





##################################
# UPDATE THE 'ALL' TRACKER TABLE #
##################################

# UPDATE ALL_LOGINS





x.execute("SELECT COUNT(*) FROM tracker")
results = x.fetchall()
for row in results:
    NUM_ROWS = row[0]



conn.commit()
NUM_ROWS = int(0 if NUM_ROWS is None else NUM_ROWS)




x.execute("UPDATE total_stats SET all_logins=%s", NUM_ROWS)
conn.commit()




# UPDATE ALL_MINUTES_ONLINE






x.execute("SELECT SUM(duration_in_min) FROM tracker")
results = x.fetchall()
for row in results:
    total_min = row[0]



conn.commit()
total_min = int(0 if total_min is None else total_min)




x.execute("UPDATE total_stats SET all_minutes_online=%s", total_min)
conn.commit()





# UPDATE ALL_USERS







x.execute("SELECT COUNT(*) FROM user_stats")
results = x.fetchall()
for row in results:
    total_users = row[0]



conn.commit()
total_users = int(0 if total_users is None else total_users)




x.execute("UPDATE total_stats SET all_users=%s", total_users)
conn.commit()





# UPDATE ALL_AVG_ONLINE







x.execute("SELECT AVG(duration_in_min) FROM tracker")
results = x.fetchall()
for row in results:
    avg = row[0]


conn.commit()
avg = int(0 if avg is None else avg)




x.execute("UPDATE total_stats SET all_avg_online=%s", avg)
conn.commit()







######################################
# CALCULATE DURATION OF EACH SESSION #
######################################





x.execute("SELECT id, username, login, logout FROM tracker WHERE duration_in_min = 1 AND logout IS NOT NULL")
results = x.fetchall()

for row in results:

    id = row[0]
    username = row[1]
    login = row[2]
    logout = row[3]

    duration_online = logout - login
    duration_in_minutes = duration_online.days * 1440 + duration_online.seconds // 60
    x2 = conn.cursor()
    x2.execute("UPDATE tracker SET duration_in_min = %s WHERE id = %s", (duration_in_minutes, id))
    conn.commit()
    x2.close()
    x5 = conn.cursor()
    x5.execute("SELECT COUNT(*) FROM tracker WHERE username = %s", (username))
    results = x5.fetchall()
    for row in results:
        total_logins = row[0]
        total_logins = int(0 if total_logins is None else total_logins)
    x3 = conn.cursor()
    x3.execute("SELECT SUM(duration_in_min) FROM tracker WHERE username = %s", (username))
    results = x3.fetchall()


    ###########################################
    # UPDATE USER_STATS LOGINS AND ONLINE MIN #
    ###########################################

    for row in results:

        online_min = row[0]
        online_min = int(0 if online_min is None else online_min)
        x4 = conn.cursor()
        x4.execute("UPDATE user_stats SET total_online_min=%s, total_logins=%s WHERE username=%s", (online_min, total_logins, username))
        conn.commit()
        x4.close()
    conn.commit()
    x3.close()

conn.commit()
x.close()

#############################################################
# COPY CURRENT MINUTE LIST TO BECOME "PREVIOUS" MINUTE LIST #
#############################################################
# We are done using the list from the previous minute, and now we write
# the current minute's list to become "previous minute" for next loop

with open("/home/war2co/scripts/users-on-this-minute.txt", "r") as f:
    lines = f.readlines()
    with open("/home/war2co/scripts/users-on-previous-minute.txt", "w") as f1:
        f1.writelines(lines)
        f1.close()
    f.close()

#######################################################
# BEGIN ANTIHACK CHECKING SECTION AND COUNTRY SCRAPER #
#######################################################

# ANTI-HACK Monitor by USA~Archer
# IMPORT WEB BROWSER
import mechanize

# IMPORT HTML PARSER
from bs4 import BeautifulSoup, NavigableString, Tag

# FOR SLEEP
import time

# MYSQL CONNECTOR IMPORT
import MySQLdb
        
br = mechanize.Browser()
# Browser options
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)

# Follows refresh 0 but not hangs on refresh > 0
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

# User-Agent
br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko')]
r = br.open('http://server.war2.ru/status/server.php')
page = r.read()
soup = BeautifulSoup(page)
table = soup.find("table")

for row in table.findAll("tr"):
    cells = row.findAll("td")
    #For each "tr", assign each "td" to a variable.
    if len(cells) > 0:
        username = cells[0].find(text=True)
        country = cells[1].find(text=True)
        time_online = cells[2].find(text=True)
        ah_status = cells[4].find(text=True)
    
        country = country.strip(' ')
        country = country.strip('\\n')
        country = country.strip('(')
        country = country.strip(')')
        country = country.lower()
        time_online = time_online.strip('\\n')
            
        if "AH" in ah_status and username != "(..)":
            ah_status = "AH"
            

            
            x = conn.cursor()
            x.execute("SELECT country FROM user_stats WHERE username = %s", (username))
            results = x.fetchall()
            for row in results:
                db_country = row[0]
                
            conn.commit()
            x.close()
                
            x2 = conn.cursor()
            x2.execute("SELECT ah_status FROM user_stats WHERE username = %s", (username))
            results = x2.fetchall()
            for row in results:
                db_ah_status = row[0]
                
            conn.commit()
            x2.close()

            # IF ITS ALREADY 'HACK!' DONT UPDATE - LEAVE HACKERS AS HACKERS FOREVER
            if db_ah_status == 'HACK!':
                continue
            
            # FOR EXAMPLE IF IT WAS NONE BUT IS NOW AH, UPDATE TO AH
            elif db_ah_status != ah_status:
                x3 = conn.cursor()
                x3.execute("UPDATE user_stats SET ah_status = %s WHERE username = %s", (ah_status,username))
                conn.commit()
                x3.close()
                
            if db_country == 'Unknown' and country is not None:
                x4 = conn.cursor()
                x4.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x4.close()
            
            elif db_country != country and country is not None:
                x5 = conn.cursor()
                x5.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x5.close()
            
        elif "none" in ah_status and username != "(..)":
            ah_status = "none"
            
            x6 = conn.cursor()
            x6.execute("SELECT country FROM user_stats WHERE username = %s", (username))
            results = x6.fetchall()
            for row in results:
                db_country = row[0]
            
            conn.commit()
            x6.close()
            
            x7 = conn.cursor()
            x7.execute("SELECT ah_status FROM user_stats WHERE username = %s", (username))
            results = x7.fetchall()
            for row in results:
                db_ah_status = row[0]
            
            conn.commit()
            x7.close()
                
            # IF ITS ALREADY 'HACK!' DONT UPDATE - LEAVE HACKERS AS HACKERS FOREVER
            if db_ah_status == 'HACK!':
                continue
            
            # FOR EXAMPLE IF IT WAS NONE BUT IS NOW AH, UPDATE TO AH
            elif db_ah_status != ah_status:
                x8 = conn.cursor()
                x8.execute("UPDATE user_stats SET ah_status = %s WHERE username = %s", (ah_status,username))
                conn.commit()
                x8.close()
                
            if db_country == 'Unknown' and country is not None:
                x9 = conn.cursor()
                x9.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x9.close()
            
            elif db_country != country and country is not None:
                x10 = conn.cursor()
                x10.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x10.close()
            
        elif "HACK!" in ah_status and username != "(..)":
            ah_status = "HACK!"
            
            x11 = conn.cursor()
            x11.execute("SELECT country FROM user_stats WHERE username = %s", (username))
            results = x11.fetchall()
            for row in results:
                db_country = row[0]
                
            conn.commit()
            x11.close()
            
            x12 = conn.cursor()
            x12.execute("SELECT ah_status FROM user_stats WHERE username = %s", (username))
            results = x12.fetchall()
            
            for row in results:
                db_ah_status = row[0]
            
            conn.commit()
            x12.close()
            
            
            x13 = conn.cursor()
            x13.execute("SELECT ah_status FROM user_stats WHERE username = %s", (username))
            results = x13.fetchall()
            for row in results:
                db_ah_status = row[0]
            
            conn.commit()
            x13.close()

            # FOR EXAMPLE IF IT WAS NONE BUT IS NOW AH, UPDATE TO AH
            if db_ah_status != ah_status:
                x14 = conn.cursor()
                x14.execute("UPDATE user_stats SET ah_status = %s WHERE username = %s", (ah_status,username))
                conn.commit()
                x14.close()
                
            if db_country == 'Unknown' and country is not None:
                x15 = conn.cursor()
                x15.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x15.close()
            
            elif db_country != country and country is not None:
                x17 = conn.cursor()
                x17.execute("UPDATE user_stats SET country = %s WHERE username = %s", (country,username))
                conn.commit()
                x17.close()
                
            
            with open("/home/war2co/scripts/hack_24hr_reset.txt", "r") as f:
                lines = [line.strip() for line in f]
                if username in lines:
                    continue
            
            
            #################################
            # APPEND TO hack_24hr_reset.txt #
            #################################


            with open("/home/war2co/scripts/hack_24hr_reset.txt", "at") as f1:
                f1.write(username + "\n")
                f1.close()
            
            x18 = conn.cursor()
            x18.execute("INSERT into hack_history (username, gametime, proof, ah_status) values (%s, UTC_TIMESTAMP(), %s, %s)", (username, page, ah_status))
            conn.commit()
            x18.close()

#######################################
# country_stats updater by USA-Archer #
#######################################


country_list = ['abw','afg','ago','aia','alb','and','ant','are','arg','arm','asm','atf','atg','aus','aut','ax','aze','bdi','bel','ben','bfa','bgd','bgr','bhr','bhs','bih','blr','blz','bmu','bol','bra','brb','brn','btn','bvt','bwa','caf','can','catalonia','cck','che','chl','chn','civ','cmr','cod','cog','cok','col','com','cpv','cri','csk','cub','cxr','cym','cyp','cze','deu','dji','dma','dnk','dom','dza','ecu','egy','england','eri','esh','esp','est','eth','europeanunion','fam','fin','fji','flk','fra','fro','fsm','gab','gbr','geo','gha','gib','gin','glp','gmb','gnb','gnq','grc','grd','grl','gtm','gu','guf','guy','hkg','hmd','hnd','hrv','hti','hun','idn','ind','iot','irl','irn','irq','isl','isr','ita','jam','jor','jpn','kaz','ken','kgz','khm','kir','kna','kor','kwt','lao','lbn','lbr','lby','lca','lie','lka','lso','ltu','lux','lva','mac','mar','mco','mda','mdg','mdv','mex','mhl','mkd','mli','mlt','mmr','mne','mng','mnp','moz','mrt','msr','mtq','mus','mwi','mys','myt','nam','ncl','ner','nfk','nga','nic','niu','nld','nor','npl','nru','nzl','omn','pak','pan','pcn','per','phl','plw','png','pol','pri','prk','prt','pry','pse','pyf','qat','reu','rou','rus','rwa','sau','scotland','sdn','sen','sgp','sgs','shn','sjm','slb','sle','slv','smr','som','spm','srb','stp','sur','svk','svn','swe','swz','syc','syr','tca','tcd','tgo','tha','tjk','tkl','tkm','tls','ton','tto','tun','tur','tuv','twn','tza','uga','ukr','umi','ury','usa','uzb','vat','vct','ven','vgb','vir','vnm','vut','wales','wlf','wsm','yem','zaf','zmb','zwe']

    
for country in country_list:
    # Find current number of users from this country
    x20 = conn.cursor()
    x20.execute("SELECT COUNT(*) FROM user_stats WHERE country = %s", country)

    results = x20.fetchall()
    for row in results:

        current_country_number = int(row[0])

    x20.close()
    
    
    # Find old number of users from this country
    x21 = conn.cursor()
    x21.execute("SELECT number FROM country_stats WHERE country = %s", country)
    
    results = x21.fetchall()
    for row in results:

        old_country_number = int(row[0])

    x21.close()
    
    # IF NEW COUNT NOT EQUAL TO OLD COUNT, UPDATE OLD COUNT
    if (current_country_number != old_country_number):
        print current_country_number
        x22 = conn.cursor()
        country_updater = "UPDATE country_stats SET number = %s WHERE country = %s"
        x22.execute(country_updater, (current_country_number, country))

        x22.close()
	conn.commit()

####################################
# AH NUMBERS UPDATER BY USA~Archer #
####################################



x799 = conn.cursor()

# FIND NUMBER OF AH_ENABLED
x799.execute("SELECT COUNT(*) FROM user_stats WHERE ah_status = 'HACK!'")



results = x799.fetchall()
for row in results:

    ah_hack = int(row[0])

x799.close()


# WRITE NUMBER OF AH_ENABLED

x899 = conn.cursor()
x899.execute("UPDATE total_stats SET ah_hack = %s", ah_hack)

x899.close()
conn.commit()


x99 = conn.cursor()

# FIND NUMBER OF AH_ENABLED
x99.execute("SELECT COUNT(*) FROM user_stats WHERE ah_status = 'AH'")



results = x99.fetchall()
for row in results:

    ah_enabled = int(row[0])

x99.close()

# WRITE NUMBER OF AH_ENABLED
x499 = conn.cursor()
x499.execute("UPDATE total_stats SET ah_enabled = %s", ah_enabled)
print 'AH Enabled'
print ah_enabled

x499.close()
conn.commit()


x299 = conn.cursor()

# FIND NUMBER OF AH_NONE
x299.execute("SELECT COUNT(*) FROM user_stats WHERE ah_status = 'none'")



results = x299.fetchall()
for row in results:

    ah_none = int(row[0])

x299.close()

# WRITE NUMBER OF AH_NONE
x599 = conn.cursor()
x599.execute("UPDATE total_stats set ah_none = %s", ah_none)
print 'AH NONE'
print ah_none

x599.close()
conn.commit()


x399 = conn.cursor()

# FIND NUMBER OF AH_UNKNOWN
x399.execute("SELECT COUNT(*) FROM user_stats WHERE ah_status = 'Unknown'")


results = x399.fetchall()
for row in results:

    ah_unknown = int(row[0])

x399.close()


# WRITE NUMBER OF AH_UNKNOWN
x699 = conn.cursor()
x699.execute("UPDATE total_stats set ah_unknown = %s", ah_unknown)
print 'AH Unknown'
print ah_unknown


x699.close()
conn.commit()
conn.close()



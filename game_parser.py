#!/usr/bin/python
# IMPORT WEB BROWSER
import mechanize

# IMPORT HTML PARSER
from bs4 import BeautifulSoup, NavigableString, Tag

# FOR TIME CALCS
from datetime import datetime, timedelta, date

# FOR RENAMING FILES
import os

# FOR SLEEP
import time

# FOR TIMESTAMPS
import datetime

# REGEX
import re

# MySQL DB Connector
import MySQLdb

# IMPORT WEB BROWSER
import mechanize

# IMPORT HTML PARSER
from bs4 import BeautifulSoup, NavigableString, Tag

# FOR TIME CALCS
from datetime import datetime, timedelta, date

# FOR RENAMING FILES
import os

# FOR SLEEP
import time

# FOR TIMESTAMPS
import datetime

# Py timezones
import pytz

# REGEX
import re

# IMPORT CONFIG / PASSWORDS
from db_config import *

try:
  # some code


	#########################################
	# SETUP MECAHNIZE TO USE AS WEB BROWSER #
	#########################################

	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

	###############  
	# GET ALL GRS #
	###############

	try:
		# User-Agent
		br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko')]
		r = br.open('http://reportb.war2.ru')
		page = r.read()
		soup = BeautifulSoup(page)
		link_list = soup.find_all('a', href=True)
	except:
		print "ERROR: Connection failed."

	for link in link_list:
		link_str = link.get('href')
		
		if link_str.startswith("gr"):
			
			with open("/home/war2co/scripts/gr_done.txt", "r") as f:
				lines = [line.strip() for line in f]
				f.close()
			
			# Check if we already did this report, if not, continue
			if link_str not in lines:
				
				gr_link = 'http://reportb.war2.ru/' + link_str
				game_time = link.findNext('td')
				game_time = game_time.contents[0]
				
				# Convert gametime from Eastern to UTC
				# UPDATE 3-22-2016 Now gametimes are already in UTC, so commented out conversion to EST and UTC
				# local = pytz.timezone('America/New_York')
				native = datetime.datetime.strptime(game_time, "%Y-%m-%d %H:%M ")
				# local_dt = local.localize(naive, is_dst=None)
				# utc_dt = native.astimezone(pytz.utc)
				game_time = native.strftime("%Y-%m-%d %H:%M:00")
			
			

				###################  
				# GET GAME REPORT #
				###################

				try:
					# User-Agent
					br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko')]
					r = br.open(gr_link)
					page = r.read()
					soup = BeautifulSoup(page)
					
				except:
					print "ERROR: Connection failed."

				############################################################
				# FILTER OUT HTML AND GIVE A PLAIN LIST OF GAMES AND DATES #
				############################################################

				gamereport = page
				gamereport = MySQLdb.escape_string(gamereport)


				name = re.findall('name=\"(.+?)\"', page)
				name = name[0]
				name = MySQLdb.escape_string(name)
				reportid = re.findall('id=(.+?)\s', page)
				reportid = reportid[0]
				reportid = MySQLdb.escape_string(reportid)

				clienttag= re.findall('clienttag=(.+?)\s', page)
				clienttag = clienttag[0]
				clienttag = MySQLdb.escape_string(clienttag)

				gametype = re.findall('type=\"(.+?)\"', page)
				gametype = gametype[0]
				gametype = MySQLdb.escape_string(gametype)

				option = re.findall('option=\"(.+?)\"', page)
				option = option[0]
				gameoption = MySQLdb.escape_string(option)

				created = re.findall('created=\"(.+?)\"', page)
				created = created[0]
				created = MySQLdb.escape_string(created)

				started = re.findall('started=\"(.+?)\"', page)
				started = started[0]
				started = MySQLdb.escape_string(started)

				ended = re.findall('ended=\"(.+?)\"', page)
				ended = ended[0]
				ended = MySQLdb.escape_string(ended)

				mapfile = re.findall('mapfile=\"(.+?)\"', page)
				mapfile = mapfile[0]
				mapfile = MySQLdb.escape_string(mapfile)

				mapauth = re.findall('mapauth=\"(.+?)\"', page)
				mapauth = mapauth[0]
				mapauth = MySQLdb.escape_string(mapauth)

				mapsize = re.findall('mapsize=(.+?)\s', page)
				mapsize = mapsize[0]
				mapsize = MySQLdb.escape_string(mapsize)

				tileset = re.findall('tileset=\"(.+?)\"', page)
				tileset = tileset[0]
				tileset = MySQLdb.escape_string(tileset)

				joins = re.findall('joins=(.+?)\s', page)
				joins = joins[0]
				joins = MySQLdb.escape_string(joins)

				maxplayers = re.findall('maxplayers=(.+?)\s', page)
				maxplayers = maxplayers[0]
				maxplayers = MySQLdb.escape_string(maxplayers)

				winners = re.findall('(.+?)WIN', page)

				if winners:
						
					winners = len(winners)

				else:
					winners = 0

				losers = re.findall('(.+?)LOSS', page)

				if losers:
					losers = len(losers)

				else:
					losers = 0

				drawers = re.findall('(.+?)DRAW', page)

				if drawers:
					drawers = len(drawers)

				else:
					drawers = 0

				discers = re.findall('(.+?)DISC', page)

				if discers:
					discers = len(discers)
					
				else:
					discers = 0

				watchers = 0
				players = 0

				x = conn.cursor()
				sql = ("INSERT into game_info(gr_link, gametime, mapfile, tileset, joins, maxplayers, reportid, gametype, clienttag, gameoption, name, mapauth, started, created, ended, mapsize, gamereport, winners, losers, discers, drawers, watchers, players) \
				values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, %s)" % \
				(gr_link, game_time, mapfile, tileset, joins, maxplayers, reportid, gametype, clienttag, gameoption, name, mapauth, started, created, ended, mapsize, gamereport, winners, losers, discers, drawers, watchers, players))
				x.execute(sql)
				gameid = x.lastrowid
				conn.commit()
				

				user_game_scores = re.findall('\":(.+?),', page, re.DOTALL)

				for user_game_score in user_game_scores:
				   
					player_stats = user_game_score

					username = re.findall('(.+?) was', player_stats)
					map(str, username)
					if username:
						username = username[0]
						username = MySQLdb.escape_string(username)
					
					else:
						username = 'Unknown'

					race = re.findall('was (.+?)\s', player_stats)
					map(str, race)
					if race:
						race = race[0]
						race = MySQLdb.escape_string(race)
					else:
						race = 'Unknown'
					
					day_in_game = 0
					hour_in_game = 0
					min_in_game = 0

					if (re.search('played for (.+?) day(.*?,)', player_stats)):

						day_in_game = re.findall('for (.+?) day.*?,', player_stats)
						day_in_game = int(day_in_game[0]) * 24 * 60
						
						hour_in_game = re.findall(', (.+?) hour.*?,', player_stats)
						hour_in_game = int(hour_in_game[0]) * 60
						
						min_in_game = re.findall(', (\d+?) minute.*?', player_stats)
						min_in_game = int(min_in_game[0]) + day_in_game + hour_in_game

					elif (re.search('played for (.+?) hour(.*?,)', player_stats)):
						hour_in_game = re.findall('for (.+?) hour.*?,', player_stats)
						hour_in_game = int(hour_in_game[0]) * 60

						min_in_game = re.findall(', (\d+?) minute.*?', player_stats)
						min_in_game = int(min_in_game[0]) + hour_in_game

						
					else:

						min_in_game = re.findall('played for (.+?) minute.*?', player_stats)
						
						if min_in_game:
							min_in_game = int(min_in_game[0])
						else:
							min_in_game = -1
					
					overall_score = re.findall('Overall Score (.+?)\n', player_stats)
					
					if overall_score:
						overall_score = int(overall_score[0])
					else:
						overall_score = -1
						
					overall_units = re.findall('(.+?) for Units', player_stats)
					
					if overall_units:
						overall_units = int(overall_units[0])
					else:
						overall_units = -1
				
					overall_structures = re.findall('(.+?) for Structures', player_stats)
					if overall_structures:
						overall_structures = int(overall_structures[0])
					else:
						overall_structures = -1
					
					overall_resources = re.findall('(.+?) for Resources', player_stats)
					if overall_resources:
						overall_resources = int(overall_resources[0])
					else:
						overall_resources = -1

					units_produced = re.findall('(.+?) Units Produced', player_stats)
					
					if units_produced:
						units_produced = int(units_produced[0])
					else:
						units_produced = -1

					units_killed = re.findall('(.+?) Units Killed', player_stats)
					if units_killed:
						units_killed = int(units_killed[0])
					else: 
						units_killed = -1

					units_lost = re.findall('(.+?) Units Lost', player_stats)
					if units_lost:
						units_lost = int(units_lost[0])
					else:
						units_lost = -1

					structures_constructed = re.findall('(.+?) Structures Constructed', player_stats)
					if structures_constructed:
						structures_constructed = int(structures_constructed[0])
					else:
						structures_constructed = -1
					
					structures_razed = re.findall('(.+?) Structures Razed', player_stats)
					if structures_razed:
						structures_razed = int(structures_razed[0])
					
					else:
						structures_razed = -1

					structures_lost = re.findall('(.+?) Structures Lost', player_stats)
					if structures_lost:
						structures_lost = int(structures_lost[0])
					else:
						structures_lost = -1

					gold_mined = re.findall('(.+?) Gold Mined', player_stats)
					if gold_mined:
						gold_mined = int(gold_mined[0])
					else:
						gold_mined = -1

					lumber_harvested = re.findall('(.+?) Lumber Harvested', player_stats)
					if lumber_harvested:
						lumber_harvested = int(lumber_harvested[0])
					else:
						lumber_harvested = -1

					
					oil_harvested = re.findall('(.+?) Oil Harvested', player_stats)
					if oil_harvested:
						oil_harvested = int(oil_harvested[0])
					else:
						oil_harvested = -1
	   
			
					total_spent = re.findall('(.+?) Total Spent', player_stats)
					if total_spent:
						total_spent = int(total_spent[0])
					else: 
						total_spent = -1

					
					x2 = conn.cursor()
					x2.execute("INSERT into user_game_score(username, overall_score, structures_razed, structures_constructed, structures_lost, gold_mined, lumber_harvested, oil_harvested, total_spent, units_killed, units_produced, units_lost, overall_resources, overall_structures, overall_units, race, gameid, min_in_game) values ('%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s', %s, %s)"%(username, overall_score, structures_razed, structures_constructed, structures_lost, gold_mined, lumber_harvested, oil_harvested, total_spent, units_killed, units_produced, units_lost, overall_resources, overall_structures, overall_units, race, gameid, min_in_game))
					conn.commit()
				

					if (units_killed == 0):
						watchers = watchers + 1
						x2 = conn.cursor()
						x2.execute("UPDATE game_info SET watchers = %s where id = %s"%(watchers, gameid))
						conn.commit()
					
					else:
						players = players + 1
						x3 = conn.cursor()
						x3.execute("UPDATE game_info SET players = %s where id = %s"%(players, gameid))
						conn.commit()
					
                                
                                # Now that we have user_game_scores, we can go back and update them with winners, losers, drawers, discers
	
				winners = re.findall('(.+?)WIN', page)

				if winners:
					for winner in winners:
						winner = winner.rstrip()
						x312 = conn.cursor()
						x312.execute("UPDATE user_game_score SET outcome = '%s' where gameid = %s and username = '%s'"%('WIN', gameid, winner))
						conn.commit()
						
						
				losers = re.findall('(.+?)LOSS', page)

				if losers:
					for loser in losers:
						loser = loser.rstrip()
						x313 = conn.cursor()
						x313.execute("UPDATE user_game_score SET outcome = '%s' where gameid = %s and username = '%s'"%('LOSS', gameid, loser))
						conn.commit()
						

				drawers = re.findall('(.+?)DRAW', page)

				if drawers:
					for drawer in drawers:
						drawer = drawer.rstrip()
						x314 = conn.cursor()
						x314.execute("UPDATE user_game_score SET outcome = '%s' where gameid = %s and username = '%s'"%('DRAW', gameid, drawer))
						conn.commit()
						

				discers = re.findall('(.+?)DISC', page)

				if discers:
					for discer in discers:
						discer = discer.rstrip()
						x315 = conn.cursor()
						x315.execute("UPDATE user_game_score SET outcome = '%s' where gameid = %s and username = '%s'"%('DISC', gameid, discer))
						conn.commit()
					
						

				with open("/home/war2co/scripts/gr_done.txt", "at") as f1:
					f1.writelines(link_str + "\n")
					f1.close
				  
				print link_str + " done."

	x9 = conn.cursor()
	x9.execute("SELECT COUNT(*) FROM game_info")
	results = x9.fetchall()
	for row in results:
			all_games = row[0]
	conn.commit()


	print 'All Games:'
	print all_games

	x99= conn.cursor()
	x99.execute("UPDATE total_stats SET all_games = %s"%(all_games))
	conn.commit()





except Exception, e:  # Bad
   print "Uncaught exception!"
   print e


